package FastJsonProjectTests;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestBigDecimalDivision {

    @Test
    public void testBigDecimalDivision() {
        BigDecimal number = BigDecimal.valueOf(0.1082);
        BigDecimal divident = BigDecimal.valueOf(0.00005);

        Assert.assertEquals(BigDecimal.ZERO, number.remainder(divident).stripTrailingZeros());
    }


    @Test
    public void testBigDecimalWith() {
        BigDecimal number = new BigDecimal("0.1082");
        BigDecimal divident = new BigDecimal("0.00005");

        Assert.assertEquals(BigDecimal.ZERO, number.remainder(divident).stripTrailingZeros());
    }

}
