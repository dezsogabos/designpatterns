package FastJsonProjectTests;

import FastJsonProject.Person;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.*;
import com.sun.xml.internal.ws.encoding.soap.SerializerConstants;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestFastJson {

    private List<Person> listOfPersons = new ArrayList<Person>();

    @Before
    public void setUp() {
        listOfPersons.add(new Person(15, "John Doe", new Date()));
        listOfPersons.add(new Person(20, "Janette Doe", new Date()));
    }

    @Test
    public void whenJavaList_thanConvertToJsonCorrect() {
        String jsonOutput= JSON.toJSONString(listOfPersons, true);
        System.out.println(jsonOutput);
    }

    @Test
    public void whenGenerateJson_thanGenerationCorrect() throws ParseException {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < 2; i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("AGE", 10);
            jsonObject.put("FULL NAME", "Doe " + i);
            jsonObject.put("DATE OF BIRTH", "2016/12/12 12:12:12");
            jsonArray.add(jsonObject);
        }
        System.out.println(JSON.toJSONString(jsonArray, true));
    }

    @Test
    public void whenJson_thanConvertToObjectCorrect() {
        Person person = new Person(20,"John Doe", new Date());
        String jsonObject = JSON.toJSONString(person);
        Person newPerson = JSON.parseObject(jsonObject, Person.class);

        assertEquals(newPerson.getAge(), 20); // if we set serialize to false
        assertEquals(newPerson.getFullName(), listOfPersons.get(0).getFullName());
    }

    @Test
    @Ignore
    public void givenContextFilter_whenJavaObject_thanJsonCorrect() {
        ContextValueFilter valueFilter = new ContextValueFilter () {
            public Object process(
                    BeanContext context, Object object, String name, Object value) {
                if (name.equals("DATE OF BIRTH")) {
                    return "NOT TO DISCLOSE";
                }
                if (value.equals("John Doe")) {
                    return ((String) value).toUpperCase();
                } else {
                    return null;
                }
            }
        };
        System.out.println(JSON.toJSONString(listOfPersons, valueFilter));
    }

    @Test
    @Ignore
    public void givenSerializeConfig_whenJavaObject_thanJsonCorrect() {
        NameFilter formatName = new NameFilter() {
            public String process(Object object, String name, Object value) {
                return name.toLowerCase().replace(" ", "_");
            }
        };

        SerializeConfig.getGlobalInstance().addFilter(Person.class,  formatName);
        System.out.println(JSON.toJSONStringWithDateFormat(listOfPersons,"yyyy-MM-dd"));
    }
}
