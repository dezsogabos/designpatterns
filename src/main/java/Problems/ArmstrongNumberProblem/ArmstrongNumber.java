package Problems.ArmstrongNumberProblem;


public class ArmstrongNumber {


    public boolean armstrongNumberDeduction(Integer integer) {
        Double armstrongResult = Double.valueOf("0");
        Integer power = integer.toString().length();
        Integer workWithThis = integer;

        for (int i =0; i<power; i++) {
            armstrongResult = armstrongResult + Math.pow(Math.floorMod(workWithThis , 10), power);
            workWithThis = Math.floorDiv(workWithThis,10);
        }
        System.out.println(armstrongResult);
        return armstrongResult.equals(integer.doubleValue());
    }

    public void iteration(Integer integer) {
        for (int i= 0; i<integer; i++ ) {
            if (armstrongNumberDeduction(i)) {
                System.out.println(i);
            }
        }

    }



}
