package RandomExercises.Generics;

// Driver class to test above
class SimpleGenericExample
{
    public static void main (String[] args)
    {
        // instance of Integer type
        Test1<Integer> iObj = new Test1<>(15);
        System.out.println(iObj.getObject());

        // instance of String type
        Test1<String> sObj =
                new Test1<>("GeeksForGeeks");
        System.out.println(sObj.getObject());
    }
}