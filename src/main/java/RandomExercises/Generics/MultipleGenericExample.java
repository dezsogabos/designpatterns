package RandomExercises.Generics;

// Driver class to test above
class MultipleGenericExample
{
    public static void main (String[] args)
    {
        Test2 <String, Integer> obj =
                new Test2<>("GfG", 15);

        obj.print();
    }
}