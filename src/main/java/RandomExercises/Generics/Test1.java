package RandomExercises.Generics;

// A Simple Java program to show working of user defined
// Generic classes

// We use < > to specify Parameter type
class Test1<T>
{
    // An object of type T is declared
    T obj;
    Test1(T obj) {  this.obj = obj;  }  // constructor
    public T getObject()  { return this.obj; }
}
