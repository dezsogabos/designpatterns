package RandomExercises;

public class HelloWorld {
    public static void main(String args[]) {
//  public : Public is an access modifier, which is used to specify who can access this method. Public means that this Method will be accessible by any Class.
//  static : It is a keyword in java which identifies it is class based i.e it can be accessed without creating the instance of a Class.
//  void : It is the return type of the method. Void defines the method which will not return any value.
//  main: It is the name of the method which is searched by JVM as a starting point for an application with a particular signature only. It is the method where the main execution occurs.
//  String args[] : It is the parameter passed to the main method.

        System.out.println("Hello World");

// Among the facilities provided by the System class
// are standard input, standard output, and error output streams;
// access to externally defined properties and environment
// variables; a means of loading files and libraries; and a utility
// method for quickly copying a portion of an array.

        /*
         * The "standard" output stream. This stream is already
         * open and ready to accept output data. Typically this stream
         * corresponds to display output or another output destination
         * specified by the host environment or user.
         */
    }
}
