package CompetitiveCodingTricks;

public class ExchangeValuesWithXOR {
    public static void main(String[] args) {

        int numberA = 7;
        int numberB = 10;

        System.out.println(numberA);
        System.out.println(numberB);
        System.out.println("\n\r");
        numberA ^= numberB;
        System.out.println(numberA);
        System.out.println(numberB);
        System.out.println("\n\r");
        numberB ^= numberA;
        System.out.println(numberA);
        System.out.println(numberB);
        System.out.println("\n\r");
        numberA ^= numberB;
        System.out.println(numberA);
        System.out.println(numberB);
    }

}
