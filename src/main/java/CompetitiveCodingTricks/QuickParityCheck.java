package CompetitiveCodingTricks;

public class QuickParityCheck {
    public static void main (String[] args) {
        int a = (int) getRandomIntegerBetweenRange(10, 20);
        System.out.println(a+" "+((a & 1) == 0 ?  "EVEN" : "ODD" ));
    }

    private static double getRandomIntegerBetweenRange(double min, double max){
        return (int)(Math.random()*((max-min)+1))+min;
    }
}
