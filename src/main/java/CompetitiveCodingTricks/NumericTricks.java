package CompetitiveCodingTricks;

import java.math.BigInteger;

public class NumericTricks {
    public static void main(String[] args) {
        System.out.println(mostSignificantDigit(423321));
        System.out.println(numberOfDigits(21312));
        System.out.println(bigIntegerGreatestCommonDivisor(2132125, 123525));
        System.out.println(isPrime(12321));
        System.out.println(isPowerOfTwo(4096));

    }

    private static int mostSignificantDigit(int n) {
        double k = Math.log10(n);
        k = k - Math.floor(k);
        return (int)Math.pow(10, k);
    }

    private static double numberOfDigits(int n) {
        return Math.floor(Math.log10(n)) + 1;
    }

    private static long bigIntegerGreatestCommonDivisor(long a, long b){
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.longValue();
    }

    private static boolean isPrime(long n) {
        return BigInteger.valueOf(n).isProbablePrime(1);
    }

    private static boolean isPowerOfTwo(int x) {
        /* First x in the below expression is
     for the case when x is 0 */
        return x!=0 && ((x&(x-1)) == 0);
    }


}
