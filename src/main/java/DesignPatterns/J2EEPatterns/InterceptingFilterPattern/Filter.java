package DesignPatterns.J2EEPatterns.InterceptingFilterPattern;

public interface Filter {
    public void execute(String request);
}