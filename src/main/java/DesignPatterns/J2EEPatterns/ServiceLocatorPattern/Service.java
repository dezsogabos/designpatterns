package DesignPatterns.J2EEPatterns.ServiceLocatorPattern;

public interface Service {
    public String getName();
    public void execute();
}