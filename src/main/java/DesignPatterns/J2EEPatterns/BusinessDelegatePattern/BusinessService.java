package DesignPatterns.J2EEPatterns.BusinessDelegatePattern;

public interface BusinessService {
    public void doProcessing();
}