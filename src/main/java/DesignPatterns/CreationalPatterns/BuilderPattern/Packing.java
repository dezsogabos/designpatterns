package DesignPatterns.CreationalPatterns.BuilderPattern;

public interface Packing {
    public String pack();
}
