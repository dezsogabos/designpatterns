package DesignPatterns.CreationalPatterns.BuilderPattern;

public interface Item {
    String name();
    Packing packing();
    float price();
}