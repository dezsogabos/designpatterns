package DesignPatterns.CreationalPatterns.BuilderPattern;

public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "Wrapper";
    }
}