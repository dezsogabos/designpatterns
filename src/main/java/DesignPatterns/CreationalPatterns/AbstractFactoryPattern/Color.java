package DesignPatterns.CreationalPatterns.AbstractFactoryPattern;

public interface Color {
    void fill();
}
