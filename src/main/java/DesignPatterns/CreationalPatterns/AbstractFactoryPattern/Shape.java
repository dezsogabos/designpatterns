package DesignPatterns.CreationalPatterns.AbstractFactoryPattern;

public interface Shape {
    void draw();
}
