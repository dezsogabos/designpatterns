package DesignPatterns.CreationalPatterns.Factory;

public interface Shape {
    void draw();
}