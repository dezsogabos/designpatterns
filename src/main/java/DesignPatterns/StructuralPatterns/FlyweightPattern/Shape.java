package DesignPatterns.StructuralPatterns.FlyweightPattern;

public interface Shape {
    void draw();
}
