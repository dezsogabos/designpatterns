package DesignPatterns.StructuralPatterns.FacadePattern;

public interface Shape {
    void draw();
}