package DesignPatterns.StructuralPatterns.DecoratorPattern;

public interface Shape {
    void draw();
}