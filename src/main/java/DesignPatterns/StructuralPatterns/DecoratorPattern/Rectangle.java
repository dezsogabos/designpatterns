package DesignPatterns.StructuralPatterns.DecoratorPattern;

public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Rectangle");
    }
}
