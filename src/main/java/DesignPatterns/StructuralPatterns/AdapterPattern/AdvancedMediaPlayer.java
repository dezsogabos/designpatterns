package DesignPatterns.StructuralPatterns.AdapterPattern;

public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}