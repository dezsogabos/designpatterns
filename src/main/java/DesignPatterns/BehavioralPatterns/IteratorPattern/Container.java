package DesignPatterns.BehavioralPatterns.IteratorPattern;

public interface Container {
    public Iterator getIterator();
}
