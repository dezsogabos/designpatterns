package DesignPatterns.BehavioralPatterns.StatePattern;

public interface State {
    public void doAction(Context context);
}