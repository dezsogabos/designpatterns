package DesignPatterns.BehavioralPatterns.CommandPattern;

public interface Order {
    void execute();
}