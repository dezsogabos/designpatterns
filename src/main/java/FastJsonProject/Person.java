package FastJsonProject;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class Person {

    @JSONField(name = "AGE")
    private int age;

   @JSONField(name = "FULL NAME", ordinal = 2)
    private String fullName;

   @JSONField(name = "DATE OF BIRTH", ordinal = 1)
    private Date dateOfBirth;

}