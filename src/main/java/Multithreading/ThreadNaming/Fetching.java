package Multithreading.ThreadNaming;

// Java program to illustrate
// how to get name of current
// executing thread.
import java.io.*;

// we can create thread by creating the
// objects of that class
class ThreadNamingInternal2 extends Thread
{

    @Override
    public void run()
    {
        // getting the current thread 's name.
        System.out.println("Fetching current thread name..");
        System.out.println(Thread.currentThread().getName());
    }
}

class Fetching
{
    public static void main (String[] args)
    {
        // creating two threads
        ThreadNamingInternal2 t1 = new ThreadNamingInternal2();
        ThreadNamingInternal2 t2 = new ThreadNamingInternal2();

        t1.start();
        t2.start();
    }
}
