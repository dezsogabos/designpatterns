package Multithreading.InterThreadCommunication;

// Java program to demonstrate inter-thread communication
// (wait(), join() and notify()) in Java
import java.util.Scanner;
public class ThreadExample
{
    public static void main(String[] args)
            throws InterruptedException
    {
        final ProduceConsumer produceConsumer = new ProduceConsumer();

        // Create a thread object that calls produceConsumer.produce()
        Thread t1 = new Thread(() -> {
            try
            {
                produceConsumer.produce();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        });

        // Create another thread object that calls
        // produceConsumer.consume()
        Thread t2 = new Thread(() -> {
            try
            {
                produceConsumer.consume();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        });

        // Start both threads
        t1.start();
        t2.start();

        // t1 finishes before t2
        t1.join();
        t2.join();
    }

    // ProduceConsumer (Produce Consumer) class with produce() and
    // consume() methods.
    public static class ProduceConsumer
    {
        // Prints a string and waits for consume()
        void produce()throws InterruptedException
        {
            // synchronized block ensures only one thread
            // running at a time.
            synchronized(this)
            {
                System.out.println("producer thread running");

                // releases the lock on shared resource
                wait();

                // and waits till some other method invokes notify().
                System.out.println("Resumed");
            }
        }

        // Sleeps for some time and waits for a key press. After key
        // is pressed, it notifies produce().
        void consume()throws InterruptedException
        {
            // this makes the produce thread to run first.
            Thread.sleep(1000);
            Scanner s = new Scanner(System.in);

            // synchronized block ensures only one thread
            // running at a time.
            synchronized(this)
            {
                System.out.println("Waiting for return key.");
                s.nextLine();
                System.out.println("Return key pressed");

                // notifies the produce thread that it
                // can wake up.
                notify();

                // Sleep
                Thread.sleep(2000);
            }
        }
    }
}