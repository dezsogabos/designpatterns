package Multithreading.Yield;

// Java program to illustrate yield() method
// in Java
import java.lang.*;

// MyThread extending Thread
class MyThread extends Thread
{
    public void run()
    {
        for (int i=0; i<5 ; i++)
            System.out.println(Thread.currentThread().getName()
                    + " in control");
    }
}